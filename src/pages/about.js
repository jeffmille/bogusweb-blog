import React, { Component } from 'react'
import Layout from '../components/layout'

export default class About extends Component {
  render() {
    return (
      <Layout>
        <h1>About Us</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Impedit commodi sunt quas sequi ex animi molestiae voluptatem! Saepe, magnam mollitia. Reprehenderit debitis dolorem sapiente, aliquam accusamus sint facilis libero rerum.</p>
      </Layout>
    )
  }
}
