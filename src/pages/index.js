import React from 'react'
import { graphql } from 'gatsby'

import Layout from '../components/layout'
import PostListing from '../components/postlisting'

const IndexPage = ({data}) => (
  <Layout>
    <h2>Posts</h2>
    {data.allMarkdownRemark.edges.map(({node}) => (
      <PostListing key={node.id} post={node} />
    ))}    
  </Layout>
)

export default IndexPage

export const query = graphql`
  query MarkdownQuery {
    allMarkdownRemark {
      edges {
        node{
          id
          frontmatter {
            title
            date (formatString: "DD MMMM YYYY")
          }
          html
          excerpt
          fields {
            slug
          }
        }
      }
    }
  }
`