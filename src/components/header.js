import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import logo from '../images/logo.svg'
import styled from 'styled-components'

export default class Header extends Component {
  componentDidUpdate = (prevProps, prevState) => {
    console.log(this.props.location.pathname)
    if (this.props.location.pathname === '/' ) {
      // console.log(this.wrapper)
    } else {
      this.wrapper.animate([
        { height: "70vh"},
        { height: "20vh"}
      ],{
        duration: 300,
        fill: "forwards",
        easing: "cubic-bezier(0.86, 0, 0.07, 1)",
        iterations: 1
      })
    }
  }
  
  render() {
    const {data} = this.props
    return (
      <HeaderWrapper ref={(wrapper) => this.wrapper = ReactDOM.findDOMNode(wrapper) }>
        <HeaderContainer>
          <h1 style={{ margin: 0 }}>
            <Link
              to="/"
              style={{
                color: 'white',
                textDecoration: 'none',
              }}
            >
              <img src={logo} alt="Level Up Logo" />
            </Link>
          </h1>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
            </ul>
          </nav>
        </HeaderContainer>
        <Img style={{
            position: 'absolute',
            left: 0,
            top: 0,
            width: '100%',
            height: '100%'
          }}
          fluid={data.background.childImageSharp.fluid}
        />
      </HeaderWrapper>
    )
  }
}

const HeaderWrapper = styled.div`
  background: #524763;
  margin-bottom: 1.45rem;
  overflow: hidden;
  position: relative;
  height: 70vh;
  h1 {
    img {
      height: 80px;
    }
  }
`
const HeaderContainer = styled.div`
  margin: 0 auto;
  max-width: 960px;
  padding: 1.45rem 1.0875rem;
  position: relative;
  z-index: 2;
`